package socialnetwork.ui;

import socialnetwork.comunitati.Comunitati;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.domain.message.Message;
import socialnetwork.domain.message.ReplyMessage;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * define a class that uses Service of Users, Service of Friendship
 */
public class UI {

    protected UtilizatorService utilizatorService;
    protected PrietenieService prietenieService;
    private MessageService messageService;
    private ReplyMessageService replyMessageService;
    private FriendshipRequestService friendshipRequestService;

    public UI(UtilizatorService utilizatorService, PrietenieService prietenieService, MessageService messageService, ReplyMessageService replyMessageService, FriendshipRequestService friendshipRequestService) {
        this.utilizatorService = utilizatorService;
        this.prietenieService = prietenieService;
        this.messageService = messageService;
        this.replyMessageService = replyMessageService;
        this.friendshipRequestService = friendshipRequestService;
    }

    public void run() throws IOException {
        int command=0;

            while (true) {
                try{
                System.out.println("0 - Close");
                System.out.println("1 - add a new User");
                System.out.println("2 - delete user");
                System.out.println("3 - add a new Prietenie");
                System.out.println("4 - delete a  Prietenie");
                System.out.println("5 - Numarul de comunitati");
                System.out.println("6 - Comunitatea cea mai mare");
                System.out.println("7 - Prieteniile unui utilizator introdus de la tastatura");
                System.out.println("8 - LunaUtilizator ");
                System.out.println("9 - Trimite un mesaj la mai multi destinatari");
                System.out.println("10 - Conversatie");
                System.out.println("11 - Printeza conversatiile a doi utilizatori in ordine cronologica");
                System.out.println("12 - Send friend request");
                System.out.println("13 - Response friendship request");
                System.out.println("14-Respond to a message: ");
                System.out.print("Introduceti o comanda: ");

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
                command = Integer.parseInt(bufferedReader.readLine());
                switch (command) {
                    case 0:
                        return;
                    case 1:
                        this.utilizatorService.addUtilizator(readUser());
                        System.out.println("Succesful add\n");
                        break;
                    case 2:
                        this.utilizatorService.deleteUtilizator(Long.parseLong(readNumber()));
                        break;
                    case 3:
                        this.prietenieService.addPrietenie(readPrietenie());
                        break;
                    case 4:
                        this.prietenieService.deletePrietenie(readIdsPrietenieToDelete());
                        break;
                    case 5:
                        Comunitati comunitati = new Comunitati((prietenieService.getAll()));
                        comunitati.printNumarDeComunitati();
                        break;
                    case 6:
                        Comunitati comunitati1 = new Comunitati((prietenieService.getAll()));
                        comunitati1.printTheMostSociableComunity();
                        break;
                    case 7:
                        afisarePrieteniiUser();
                        break;
                    case 8:
                        afisarePrieteniiUser2();
                        break;
                    case 9:
                        sendMessageToMany();
                        break;
                    case 10:
                        addConversation();
                        break;
                    case 11:
                        printConversation();
                        break;
                    case 12:
                        sendFriendshipRequest();
                        break;
                    case 13:
                        answerFriendshipRequest();
                        break;
                    case 14:
                        respondMessage();
                        break;
                }
            }catch(ValidationException e) {
                    System.err.println(e.getMessage());
            }
        }
    }

    private void respondMessage() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String idUser;
        while(true){
            System.out.print("Introduce the id of the user: ");
            idUser=bufferedReader.readLine();
            try{
                Long.parseLong(idUser);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        Long idUserLong=Long.parseLong(idUser);

        Iterable<Message> filter=messageService.getMessageToUser(idUserLong);
        filter.forEach(x->{
            System.out.println("mesaj: "+x.getId() +" | From: id user "+x.getFrom().getId()+" "+
                    x.getFrom().getFirstName()+" "+x.getFrom().getLastName()+" | Message: "+x.getMessage()
                    );
        });
        System.out.println("Which message do yu want to answer? ");
        String number;
        while(true){
            System.out.print("Please enter the id: ");
            number=bufferedReader.readLine();
            try{
                Long.parseLong(number);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        Long numberLong=Long.parseLong(number);
        System.out.println("Introduce the message: ");
        String text=bufferedReader.readLine();

        Message msgFrom=messageService.getMessage(numberLong);
        Utilizator user = utilizatorService.findOne(idUserLong);
        Utilizator userFrom=msgFrom.getFrom();
        Message response=new Message(user,Arrays.asList(userFrom),text,LocalDateTime.now());
        messageService.addMessage(response);

    }

    private void answerFriendshipRequest() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String idUser;
        while(true){
            System.out.print("Introduce the id of the user: ");
            idUser=bufferedReader.readLine();
            try{
                Long.parseLong(idUser);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        Long idUserLong=Long.parseLong(idUser);
        Iterable<FriendshipRequest> friendshipRequestIterable=friendshipRequestService.getAllPendingRequests(Long.parseLong(idUser));

        if(!friendshipRequestIterable.iterator().hasNext()){
            System.out.println("There are no pending friendship requests!\n");
            return;
        }
        friendshipRequestIterable.forEach(System.out::println);

        System.out.println("Which friendship do you chose?\n");
        String ID;
        while(true){
            System.out.print("Introduce the id of the friendship request you want to respond to: ");
            ID=bufferedReader.readLine();
            try{
                Long.parseLong(ID);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        Long IDLong=Long.parseLong(ID);
        System.out.println("Accept friendship request:(A) | Decline friendship request:(D)\n");
        String action=bufferedReader.readLine();

        FriendshipRequest friendshipRequest=friendshipRequestService.findOne(IDLong);
        if(action.equals("A") || action.equals("a")){

            friendshipRequestService.deleteRequest(IDLong);
            friendshipRequest.setStatus("approved");
            friendshipRequestService.addRequest(friendshipRequest);
            Long idUserFrom=friendshipRequest.getFrom().getId();
            Prietenie prietenie=new Prietenie(new Tuple<>(idUserFrom,idUserLong));
            prietenieService.addPrietenie(prietenie);
            System.out.println("Accepted succesfull!\n");
        } else if(action.equals("D") || action.equals("d")){
            friendshipRequestService.deleteRequest(Long.parseLong(ID));
            friendshipRequest.setStatus("rejected");
            System.out.println("Rejected succesfull!\n");
            friendshipRequestService.addRequest(friendshipRequest);
        }else{
            System.out.println("Invalid action! Choose between A and D");
        }

    }

    private void sendFriendshipRequest() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String idSender;
        while(true){
            System.out.print("Introduce the id of the fsender: ");
            idSender=bufferedReader.readLine();
            try{
                Long.parseLong(idSender);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }

        String idReceiver;
        while(true){
            System.out.print("Introduce the id of the receiver: ");
            idReceiver=bufferedReader.readLine();
            try{
                Long.parseLong(idReceiver);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        Long idSenderLong=Long.parseLong(idSender);
        Long idReceiverLong=Long.parseLong(idReceiver);

        System.out.print("Introduce the message: ");
        String message=bufferedReader.readLine();
        Utilizator userSender=utilizatorService.findOne(idSenderLong);
        Utilizator userReceiver = utilizatorService.findOne(idReceiverLong);

        if(userSender!=null || userReceiver!=null){
            FriendshipRequest friendshipRequest=new FriendshipRequest(userSender,Arrays.asList(userReceiver),message,
                    LocalDateTime.now(),"pending");
            friendshipRequestService.addRequest(friendshipRequest);
        }
        else
            System.out.println("Invalid user id\n");

    }


    private void printConversation() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String idLeft;
        while(true){
            System.out.print("Introduce the id of the first user: ");
            idLeft=bufferedReader.readLine();
            try{
                Long.parseLong(idLeft);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }

        String idRight;
        while(true){
            System.out.print("Introduce the id of the second user: ");
            idRight=bufferedReader.readLine();
            try{
                Long.parseLong(idRight);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        Long idLeftLong=Long.parseLong(idLeft);
        Long idRightLong=Long.parseLong(idRight);
        Iterable<ReplyMessage> conversation=replyMessageService.getConversation(idLeftLong,idRightLong);
        if(!conversation.iterator().hasNext())
            System.out.println("There are no conversations between the two");
        else
            conversation.forEach(System.out::println);
    }
    private void addConversation() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String idFrom;
        while(true){
            System.out.println("Introduce the id of the first user: ");
            idFrom=bufferedReader.readLine();
            try{
                Long.parseLong(idFrom);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        String idTo;
        while(true){
            System.out.println("Introduce the id of the second user: ");
            idTo=bufferedReader.readLine();
            try{
                Long.parseLong(idTo);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        Long idFromLong=Long.parseLong(idFrom);
        Long idToLong=Long.parseLong(idTo);
        Utilizator sender = utilizatorService.findOne(idFromLong);
        Utilizator receiver = utilizatorService.findOne(idToLong);
        if(sender!=null || receiver!=null){
            System.out.print("Introduce the message: ");
            String messageText= bufferedReader.readLine();
            ReplyMessage replyMessage=new ReplyMessage(sender, Arrays.asList(receiver),messageText,LocalDateTime.now(),replyMessageService.getReplyMessage(0L));
            replyMessageService.addMessage(replyMessage);
            while(true){
                System.out.print("Do you want to continue the conversation? [Y/N]");
                String response=new BufferedReader(new InputStreamReader(System.in)).readLine();
                if(response.equals("Y")||response.equals("y")){
                    Utilizator auxiliaryUser=sender;
                    sender=receiver;
                    receiver=auxiliaryUser;
                    System.out.print("Introduce the text message= ");
                    messageText= bufferedReader.readLine();
                    replyMessage=new ReplyMessage(sender, Arrays.asList(receiver),messageText,LocalDateTime.now(),replyMessageService.getReplyMessage(replyMessage.getId()));
                    replyMessageService.addMessage(replyMessage);
                }else if(response.equals("N")||response.equals("n"))
                    break;

            }
        }
        else{
            System.out.println("Utilizator inexistent!\n");
        }


    }
    private void sendMessageToMany() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String idFrom;
        long leftIdLong=0;
        while(true){
            System.out.println("Introduce the id of the first user: ");
            idFrom=bufferedReader.readLine();
            try{
                Long.parseLong(idFrom);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }

        String messageSent="";
        System.out.println("Introduce the message: ");
        messageSent=bufferedReader.readLine();
        LocalDateTime data= LocalDateTime.now();
        List<Utilizator> listTo=new ArrayList<>();
        String idUser="";
        System.out.println("Introduce the ids of users to whom the message is sent. To end the process, introduce 0: ");
        while(true) {
            while (true) {
                try {
                    idUser = bufferedReader.readLine();
                    Long.parseLong(idUser);
                    break;
                } catch (NumberFormatException | IOException e) {
                    System.err.println("ID invalid! Introduceti un id nou: ");
                }
            }
            Long idd = Long.parseLong(idUser);
            if (idd == 0) break;
            else{
                Utilizator user=utilizatorService.findOne(idd);
                if(user!=null)
                    listTo.add(utilizatorService.findOne(idd));
                else
                    System.out.println("Invalid userTo");
            }

        }
        Utilizator utilizatorFrom =  utilizatorService.findOne(Long.parseLong(idFrom));
        Message mesajCreat=new Message(utilizatorFrom,listTo,messageSent,data);
        if (utilizatorFrom == null) {
            System.out.println("Invalid userFrom");
        }else{
            messageService.addMessage(mesajCreat);
        }


    }

    /**
     * @return a Tuple of <Long,Long>
     * @throws IOException if the id of the first user is not a number
     *                     if the id of the second user is not anumber
     */
    private Tuple<Long, Long> readIdsPrietenieToDelete() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String leftIdString;
        long leftIdLong=0;
        while(true){
            System.out.println("Introduce the id of the first user: ");
            leftIdString=bufferedReader.readLine();
            try{
                leftIdLong=Long.parseLong(leftIdString);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        String rightIdString;
        long rightIdLong=0;
        while(true){
            System.out.println("Introduce the id of the second user: ");
            rightIdString=bufferedReader.readLine();
            try{
                rightIdLong=Long.parseLong(rightIdString);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number");
            }
        }

        return new Tuple<>(leftIdLong,rightIdLong);
    }

    /**
     * @return a friendship
     * @throws IOException if the id of the first user is not a number
     *                     if the id of the second user is not a number
     */
    private Prietenie readPrietenie() throws IOException {
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(System.in)));
        String leftIdString;
        long leftIdLong=0;
        while(true){
            System.out.println("Introduce the id of the first user: ");
            leftIdString=bufferedReader.readLine();
            try{
                leftIdLong=Long.parseLong(leftIdString);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number!");
            }
        }
        String rightIdString;
        long rightIdLong=0;
        while(true){
            System.out.println("Introduce the id of the second user: ");
            rightIdString=bufferedReader.readLine();
            try{
                rightIdLong=Long.parseLong(rightIdString);
                break;
            }catch(NumberFormatException e){
                System.out.println("Introduce a valid id - a number");
            }
        }
        Prietenie prietenie=new Prietenie(new Tuple<>(leftIdLong,rightIdLong));

        return prietenie;
    }

    /**
     * a method that reads strings (id,name,lastname) using bufferedReader and create a use
     * @return a user that contains the first name and the last name
     * @throws IOException if the id is invalid
     */

    protected void afisarePrieteniiUser2() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((System.in)));
        String idUser;

        System.out.print("Introduce the id of the user: ");
        while(true){

            try {
                idUser = bufferedReader.readLine();
                Long.parseLong(idUser);
                break;
            }catch(NumberFormatException | IOException e){
                System.err.println("ID invalid! Introduceti un id nou: ");
            }
        }
        String month;
        System.out.println("Introduce the month: ");
        while(true){
            try{
                month = bufferedReader.readLine();
                Long.parseLong(month);
                break;
            }catch (NumberFormatException|IOException e) {
                System.err.println("Invalid Month! Introduce a number between 01 and 12: ");
            }
        }


        System.out.println("Introduce the year: ");
        String year=bufferedReader.readLine();
        Long yearLong=Long.parseLong(year);

        Long idUserLong = Long.parseLong((idUser));
        List<Prietenie> listaprieteni = new ArrayList<Prietenie>();
        Iterable<Prietenie> prietenieIterable = prietenieService.getAll();

        prietenieIterable.forEach(listaprieteni::add);

        String finalMonth = month;
        listaprieteni.stream()
                .filter(x->{
                    return x.getId().getRight().equals(idUserLong) || x.getId().getLeft().equals(idUserLong);

                })
                .filter(x->{

                   String date=x.getDate().toString();
                   String dateCompare=year+"-"+finalMonth;
                   return date.substring(0,7).equals(dateCompare);
                })
                .forEach(x->{
                    Utilizator utilizator=null;
                    if(x.getId().getLeft().equals(idUserLong)){
                        utilizator=utilizatorService.findOne(x.getId().getRight());
                    }
                    else{
                        utilizator=utilizatorService.findOne(x.getId().getLeft());
                    }
                    System.out.println(utilizator.getFirstName()+" | "+utilizator.getLastName()+" | " +x.getDate());
                });

    }
    protected void afisarePrieteniiUser(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((System.in)));
        String idUser;

        System.out.print("Introduce the id of the user: ");
        while(true){

            try {
                idUser = bufferedReader.readLine();
                Long.parseLong(idUser);
                break;
            }catch(NumberFormatException | IOException e){
                System.err.println("ID invalid! Introduceti un id nou: ");
            }
        }
        Long idUserLong = Long.parseLong((idUser));

        List<Prietenie> listaprieteni = new ArrayList<Prietenie>();
        Iterable<Prietenie> prietenieIterable = prietenieService.getAll();

        prietenieIterable.forEach(listaprieteni::add);

        listaprieteni.stream()
                .filter(x->{
                    return x.getId().getRight().equals(idUserLong) || x.getId().getLeft().equals(idUserLong);

                })

                .forEach(x->{
                    Utilizator utilizator=null;
                    if(x.getId().getLeft().equals(idUserLong)){
                       utilizator=utilizatorService.findOne(x.getId().getRight());
                    }
                    else{
                        utilizator=utilizatorService.findOne(x.getId().getLeft());
                    }
                    System.out.println(utilizator.getFirstName()+" | "+utilizator.getLastName()+" | " +x.getDate());
                });


    }

    protected Utilizator readUser() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((System.in)));
        String idUser;


        System.out.print("Introduce the id of the user: ");
        while(true){

            try {
                idUser = bufferedReader.readLine();
                Long.parseLong(idUser);
                break;
            }catch(NumberFormatException e){
                System.err.println("ID invalid! Introduceti un id nou: ");
            }
        }

        String firstNameUser;
        System.out.print("Introduce the first name of the user: ");
        firstNameUser = bufferedReader.readLine();

        String lastNameUser;
        System.out.print("Introduce the last name of a user: ");
        lastNameUser=bufferedReader.readLine();

        Utilizator utilizator=new Utilizator(firstNameUser,lastNameUser);
        utilizator.setId(Long.parseLong(idUser));

        return utilizator;

    }

    /**
     * @return an id of type Integer
     * @throws IOException if the id is invalid
     */
    private String readNumber(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((System.in)));
        String idUser;
        System.out.println("Introduce an id user: ");
        while(true){

            try {

                idUser = bufferedReader.readLine(); //citim id-ul
                Long.parseLong(idUser);             //il convertim la long
                break;
            }catch(NumberFormatException | IOException e){
                System.err.println("ID invalid! Introduceti un id nou: ");
            }
        }
        return idUser;
    }
}
