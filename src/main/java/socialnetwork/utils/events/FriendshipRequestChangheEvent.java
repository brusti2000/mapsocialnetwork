package socialnetwork.utils.events;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.message.FriendshipRequest;

public class FriendshipRequestChangheEvent implements Event {
    private ChangeEventType changeEventType;
    private FriendshipRequest oldFriendshipRequest,newFriendshipRequest;

    public FriendshipRequestChangheEvent(ChangeEventType changeEventType, FriendshipRequest oldFriendshipRequest, FriendshipRequest newFriendshipRequest) {
        this.changeEventType = changeEventType;
        this.oldFriendshipRequest = oldFriendshipRequest;
        this.newFriendshipRequest = newFriendshipRequest;
    }

    public FriendshipRequestChangheEvent(ChangeEventType changeEventType, FriendshipRequest oldFriendshipRequest) {
        this.changeEventType = changeEventType;
        this.oldFriendshipRequest = oldFriendshipRequest;
    }

    public ChangeEventType getChangeEventType() {
        return changeEventType;
    }

    public FriendshipRequest getOldFriendshipRequest() {
        return oldFriendshipRequest;
    }

    public FriendshipRequest getNewFriendshipRequest() {
        return newFriendshipRequest;
    }
}
