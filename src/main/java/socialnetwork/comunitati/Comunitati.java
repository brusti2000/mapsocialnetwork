package socialnetwork.comunitati;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * define a class that uses an Adjacent List, a Map for the visited vertexes, the max number of vertexes, an Iterable List of Friendships
 */
public class Comunitati {

   private ArrayList<ArrayList<Long>> ListaDeAdiacenta=new ArrayList<ArrayList<Long>>();
   private Map<Long,Long> visited=new HashMap<>();
   private int V=100;
   private Iterable<Prietenie> listaPrietenii;

    /**
     * @param listaPrietenii - Iterable
     *  creates an Adjacent List for each vertex
     */
    public Comunitati(Iterable<Prietenie> listaPrietenii) {
         for(int i=0;i<V;i++){
             ListaDeAdiacenta.add(new ArrayList<Long>());
         }
        this.listaPrietenii=listaPrietenii;
         listaPrietenii.forEach(prietenie->{
             ListaDeAdiacenta.get(prietenie.getId().getLeft().intValue()).add(prietenie.getId().getRight());
             ListaDeAdiacenta.get(prietenie.getId().getRight().intValue()).add(prietenie.getId().getLeft());
             visited.put(prietenie.getId().getLeft(),0L);
             visited.put(prietenie.getId().getRight(),0L);
         });

    }

    /**
     * @param vertex - type Long
     * @return - the number of vertexes from the 'connected component'
     */
    private int DFS(Long vertex) {
        visited.put(vertex, 1L);
        int numarulDePrietenii = 1;
        for (Long child : ListaDeAdiacenta.get(vertex.intValue())) {
            if (visited.get(child) == 0) {
                numarulDePrietenii = numarulDePrietenii + DFS(child);
            }
        }
        return numarulDePrietenii; //nr de varfuri din componenta conexa
    }

    /**
     * define a method that counts the number of the 'connected components'
     */
    public void printNumarDeComunitati(){
        int nrDeComunitati=0; //numarul de componente conexe
        for(long vertex: visited.keySet()){
            if(visited.get(vertex)==0){
                DFS(vertex);
                nrDeComunitati++;
            }
        }
        System.out.println("Numarul de comunitatie este egal cu " + nrDeComunitati);

    }
//numarul de componente conexe
    /**
     *define a method that resets the value of each vertex from the Map to 0 = unvisited
     */
    public void resetVisited() {
        listaPrietenii.forEach(prietenie -> {
            visited.put(prietenie.getId().getLeft(), 0L);
            visited.put(prietenie.getId().getRight(), 0L);
        });
    }

    /**
     *
     */
    public void printTheMostSociableComunity(){
        int numberOfPeopleMax=0; //nr maxim de persoane dintr-o comunitate
        long representMaxComunity=0; //un reprezentatnt al cmunitatii maxime ca sa fac DFS
       //aflu comunitatea cu cele mai multe prietenii = cea mai mare componenta conexa
        for(Long vertex:visited.keySet()){
            if(visited.get(vertex)==0){
                int numberOfPeople=DFS(vertex);
                if(numberOfPeopleMax<numberOfPeople){
                    numberOfPeopleMax=numberOfPeople;
                    representMaxComunity=vertex;
                }
            }
        }
        resetVisited();
        DFS(representMaxComunity); //DFS pe un id din comunitatea maxima ca sa afisez toti oamenii din comunitatea maxima
        System.out.println("The most sociable comunity has "+ numberOfPeopleMax + " members:");
        for(Long vertex:visited.keySet()){
            if(visited.get(vertex)==1){
                System.out.println(vertex+" ");
            }
        }
        System.out.println();
    }

}
