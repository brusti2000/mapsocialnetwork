package socialnetwork;

import socialnetwork.GUI.MainFX;
import socialnetwork.comunitati.Comunitati;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.domain.message.Message;
import socialnetwork.domain.message.ReplyMessage;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.*;
import socialnetwork.service.*;
import socialnetwork.service.validator.ValidatorFriendshipRequestService;
import socialnetwork.ui.UI;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
//        String fileNameUsers=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
//        String fileNamePrietenii=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.prietenii");
//        String fileNameMessages=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.mesaje");
//        String fileNameConversation=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.conversatii");
//        String fileNameFriendshipRequest=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.friendshiprequest");

//        Repository<Long,Utilizator> userFileRepository = new UtilizatorFile(fileNameUsers, new UtilizatorValidator(),true);
//        Repository<Tuple<Long,Long>,Prietenie> prietenieFilerepository=new PrietenieFile(fileNamePrietenii,new PrietenieValidator(userFileRepository),userFileRepository,true);

       // UtilizatorService utilizatorService=new UtilizatorService(userFileRepository,prietenieFilerepository);
       // PrietenieService prietenieService=new PrietenieService(prietenieFilerepository,userFileRepository);
//       Comunitati comunitati=new Comunitati(prietenieService.getAll());
//       comunitati.printNumarDeComunitati();

//       Repository<Long, Message> messageRepository=new MessageFileRepository(fileNameMessages,
//               new MessageValidator(),false, userFileRepository);
//       Repository<Long, ReplyMessage> replyMessageRepository=new ReplyMessageFilerepository(fileNameConversation,
//               new ReplyMessageValidator(),true,userFileRepository);
//       Repository<Long, FriendshipRequest> friendshipRequestRepository=new FriendshipRequestRepository(fileNameFriendshipRequest,new FriendshipRequestValidator(),true,userFileRepository);

       // MessageService messageService=new MessageService(messageRepository);
     //   ReplyMessageService replyMessageService=new ReplyMessageService(replyMessageRepository);

      // FriendshipRequestService friendshipRequestService=new FriendshipRequestService(friendshipRequestRepository,new ValidatorFriendshipRequestService<>());


       MainFX.main(args);

       //        UI ui=new UI(utilizatorService,prietenieService, messageService,replyMessageService,friendshipRequestService);
//        ui.run();
//        //print outpu
//        userFileRepository.findAll().forEach(System.out::println);
//        prietenieFilerepository.findAll().forEach(System.out::println);
//        replyMessageRepository.findAll().forEach(System.out::println);
//        friendshipRequestRepository.findAll().forEach(System.out::println);

    }
}


