package socialnetwork.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controllers.IntroducereController;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.domain.message.Message;
import socialnetwork.domain.message.ReplyMessage;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.*;
import socialnetwork.service.*;
import socialnetwork.service.validator.ValidatorFriendshipRequestService;

import java.io.IOException;

public class MainFX extends Application {
    private static UtilizatorService utilizatorService;
    private static  PrietenieService prietenieService;
    private static  MessageService messageService;
    private static  ReplyMessageService replyMessageService;
    private static    FriendshipRequestService friendshipRequestService;

    @Override
    public void start(Stage primaryStage) throws Exception {
            initView(primaryStage);
            primaryStage.setWidth(600);
            primaryStage.setTitle("Welcome");
            primaryStage.show();
    }

    public static void main(String[] args){
        String fileNameUsers= ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
        String fileNamePrietenii=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.prietenii");
        String fileNameMessages=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.mesaje");
        String fileNameConversation=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.conversatii");
        String fileNameFriendshipRequest=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.friendshiprequest");

        Repository<Long, Utilizator> userFileRepository = new UtilizatorFile(fileNameUsers, new UtilizatorValidator(),true);
        Repository<Tuple<Long,Long>, Prietenie> prietenieFilerepository=new PrietenieFile(fileNamePrietenii,new PrietenieValidator(userFileRepository),userFileRepository,true);
        Repository<Long, Message> messageRepository=new MessageFileRepository(fileNameMessages,
                new MessageValidator(),false, userFileRepository);
        Repository<Long, ReplyMessage> replyMessageRepository=new ReplyMessageFilerepository(fileNameConversation,
                new ReplyMessageValidator(),true,userFileRepository);
        Repository<Long, FriendshipRequest> friendshipRequestRepository=new FriendshipRequestRepository(fileNameFriendshipRequest,new FriendshipRequestValidator(),true,userFileRepository);

        utilizatorService=new UtilizatorService(userFileRepository,prietenieFilerepository);
        prietenieService=new PrietenieService(prietenieFilerepository,userFileRepository);
        messageService=new MessageService(messageRepository);
        replyMessageService=new ReplyMessageService(replyMessageRepository);
        friendshipRequestService=new FriendshipRequestService(friendshipRequestRepository,new ValidatorFriendshipRequestService(),prietenieFilerepository);

        launch(args);
    }

    private void initView(Stage primaryStage) throws IOException {
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/introducere.fxml"));
        AnchorPane layout = loader.load();
        primaryStage.setScene(new Scene(layout));
        IntroducereController introducereController=loader.getController();
        introducereController.setUserService(utilizatorService,primaryStage);
        introducereController.setFriendshipService(prietenieService);
        introducereController.setFriendshipRequestService(friendshipRequestService);
        introducereController.setMessageService(messageService);

    }
}
