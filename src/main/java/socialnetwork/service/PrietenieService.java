package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.service.validator.ValidatorPrietenieService;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * define a class which uses a Validator of friendship,
 *                           a Repository for Users and
 *                           a Repository for Friendships
 */
public class PrietenieService implements Observable<FriendshipChangeEvent> {
    private Repository<Tuple<Long,Long>, Prietenie> repository;
    private Repository<Long,Utilizator> repositoryUtilizator;
    private ValidatorPrietenieService<Prietenie> prietenieValidatorPrietenieService=new ValidatorPrietenieService<>();
    private  List<Observer<FriendshipChangeEvent>> observers=new ArrayList<>();

    /**
     * @param repository -
     * @param repositoryUtilizator -
     */
    public PrietenieService(Repository<Tuple<Long, Long>, Prietenie> repository, Repository<Long, Utilizator> repositoryUtilizator) {
        this.repository = repository;
        this.repositoryUtilizator = repositoryUtilizator;
    }

    /**
     * @param prietenieParam -  must be not null
     * @return null- if the given entity is saved
     *             - otherwise returns the entity (id already exists)
     * @throws ValidationException if the entity is not valid
     */
    public Prietenie addPrietenie(Prietenie prietenieParam){
      Prietenie prietenie = repository.save(prietenieParam);

      prietenieValidatorPrietenieService.validateAdd(prietenie);

      Utilizator utilizatorLeft = repositoryUtilizator.findOne(prietenieParam.getId().getLeft());
      Utilizator utilizatorRight = repositoryUtilizator.findOne(prietenieParam.getId().getRight());
        utilizatorLeft.getFriends().add(utilizatorRight);
        utilizatorRight.getFriends().add(utilizatorLeft);

        if(prietenie==null)
        {
            notifyObserver(new FriendshipChangeEvent(ChangeEventType.ADD,prietenie));
        }
        return prietenie;
    }

    /**
     * @param ids - id must be not null
     * @return - the removed entity or null if there is no entity with the given id
     * @throws  ValidationException if the friendship does not exist
     */
    //delete:returneaza null daca nu exista
    public Prietenie deletePrietenie(Tuple<Long,Long> ids) {
        Prietenie prietenie=repository.delete(ids);
        prietenieValidatorPrietenieService.validateDelete(prietenie);
        if(prietenie!= null){
            notifyObserver(new FriendshipChangeEvent(ChangeEventType.DELETE,prietenie));
        }
        return repository.delete(ids);
    }

    /**
     * @return all entities
     */
    public Iterable<Prietenie> getAll(){
        return repository.findAll();
    }

    public Iterable<Prietenie> getAllFriendshipUser(Long userID){
        Iterable<Prietenie> allFriendships = this.getAll();
        List<Prietenie> listFriendshipUser = new ArrayList<>();
        allFriendships.forEach(friendship->{
            if(friendship.getId().getLeft().equals(userID))
                listFriendshipUser.add(friendship);
            else
                if(friendship.getId().getRight().equals(userID))
                    listFriendshipUser.add(friendship);
        });
        return listFriendshipUser;
    }

    public Prietenie findOne(Long idLeft, Long idRight){
        return repository.findOne(new Tuple<>(idLeft,idRight));
    }

    public Iterable<Prietenie> getAllNonFriendshipUser(Long userId){
        Iterable<Prietenie> allFriendships = this.getAll();
        List<Prietenie> listNonFriendshipUser = new ArrayList<>();
        allFriendships.forEach(friendship->{
            if(!friendship.getId().getLeft().equals(userId)&& !friendship.getId().getRight().equals(userId))
                listNonFriendshipUser.add(friendship);
        });
        return listNonFriendshipUser;
    }

    @Override
    public void addObserver(Observer<FriendshipChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipChangeEvent> e) {
        //observers.add(e);
    }

    @Override
    public void notifyObserver(FriendshipChangeEvent t) {
        observers.stream().forEach(x->x.update(t));
    }
}
