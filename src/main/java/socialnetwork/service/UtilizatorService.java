package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dtos.UserDTO;
import socialnetwork.repository.Repository;
import socialnetwork.service.validator.Validator;
import socialnetwork.service.validator.ValidatorUserService;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * define a class which uses a Validator of users,
 *                           a Repository for Users and
 *                           a Repository for Friendships
 */
public class UtilizatorService  {
    private final Repository<Long, Utilizator> repoUtilizator;
    private final Repository<Tuple<Long,Long>, Prietenie> repoPrietenie;
    private final Validator<Utilizator> validatorUserService=new ValidatorUserService<>();

    /**
     * @param repoUtilizator -
     * @param repoPrietenie  -
     */
    public UtilizatorService(Repository<Long, Utilizator> repoUtilizator, Repository<Tuple<Long, Long>, Prietenie> repoPrietenie) {
        this.repoUtilizator = repoUtilizator;
        this.repoPrietenie = repoPrietenie;
    }

    /**
     * @param utilizator - type Utilizator, entity that has to be added
     * @return user of type Utilizator
     */
    public Utilizator addUtilizator(Utilizator utilizator) {
        Utilizator user = repoUtilizator.save(utilizator);
        //facem validarea in functie de ce ne returneaza .save
        validatorUserService.validateAdd(user);
        return user;
    }

    /**
     * method that removes the user with id given as a parameter and
     * its friendships
     * @param id - Long, the unique identifier of the user to be deleted
     * @return user that has been removed
     */
    public Utilizator deleteUtilizator(Long id) {
        //3;4
        //3;5
        //6;3
        //sterge in toate prieteniile care il contin pe 3
        Utilizator utilizator = repoUtilizator.delete(id);
        validatorUserService.validateDelete(utilizator);
        utilizator.getFriends().forEach((x)->{
            if(repoPrietenie.findOne(new Tuple<>(x.getId(),id)) != null){
                repoPrietenie.delete(new Tuple<>(x.getId(),id));
            }

            if(repoPrietenie.findOne(new Tuple<>(id,x.getId())) != null){
                repoPrietenie.delete(new Tuple<>(id,x.getId()));
            }
        });

        return utilizator;
    }

    /**
     * @return all the users
     */
    public Iterable<Utilizator> getAll(){
        return repoUtilizator.findAll();
    }
    public Utilizator findOne(Long ID){
        return repoUtilizator.findOne(ID);
    }

    /**
     * @return
     */
    public List<UserDTO> getAllUserDTO(){
        List<UserDTO> userDTOList = new ArrayList<>();
        getAll().forEach(user->{
            UserDTO userDTO=new UserDTO(user.getFirstName(),user.getLastName());
            userDTO.setId(user.getId());
            userDTOList.add(userDTO);
        });
        return userDTOList;
    }

    public UserDTO getUserDTO(Long userID) {
        Utilizator user=findOne(userID);//id,firstname,lastname.listPrietenii
        UserDTO userDTO = new UserDTO(user.getFirstName(),user.getLastName());
        userDTO.setId(userID);
        return userDTO;
    }
}
