package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.repository.Repository;
import socialnetwork.service.validator.Validator;
import socialnetwork.service.validator.ValidatorFriendshipRequestService;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.FriendshipRequestChangheEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
//sa notificam tabelul cand facem modificari in service
public class FriendshipRequestService implements Observable<FriendshipRequestChangheEvent> {
    private List<Observer<FriendshipRequestChangheEvent>>observers=new ArrayList<>();
    private  Repository<Long, FriendshipRequest> requestRepository;
    private ValidatorFriendshipRequestService friendshipRequestValidator = new ValidatorFriendshipRequestService();
    private Repository<Tuple<Long, Long>, Prietenie> friendshipRepository;

    public FriendshipRequestService(Repository<Long, FriendshipRequest> requestRepository, ValidatorFriendshipRequestService friendshipRequestValidator,
                                    Repository<Tuple<Long,Long>,Prietenie> friendshiprepository) {
        this.requestRepository = requestRepository;
        this.friendshipRequestValidator = friendshipRequestValidator;
        this.friendshipRepository=friendshiprepository;
    }

    public FriendshipRequest addRequest(FriendshipRequest friendshipRequest){
        friendshipRequestValidator.validateBeforeAdding(friendshipRequest,getAll(),friendshipRepository.findAll());
        FriendshipRequest friendshipRequest1= requestRepository.save(friendshipRequest);

        return friendshipRequest1;
    }

    public FriendshipRequest deleteRequest(Long id){
        FriendshipRequest friendshipRequest= requestRepository.delete(id);
        if(friendshipRequest!=null) notifyObserver(new FriendshipRequestChangheEvent(ChangeEventType.DELETE,friendshipRequest));
       // friendshipRequestValidator.validateDelete(friendshipRequest);
        return friendshipRequest;
    }


    public Iterable<FriendshipRequest> getAllPendingRequests(Long userID){
        Iterable<FriendshipRequest> list = requestRepository.findAll();
        List<FriendshipRequest> listPendingFriendshiprequest=new ArrayList<FriendshipRequest>();
        list.forEach(request->{
            if(request.getStatus().equals("pending") && request.getTo().get(0).getId().equals(userID)){
                listPendingFriendshiprequest.add(request);
            }
        });
        return listPendingFriendshiprequest;
    }//from
    public List<FriendshipRequest> getAllRequests(Long userID){
        Iterable<FriendshipRequest> list = requestRepository.findAll();
        List<FriendshipRequest> listPendingFriendshiprequest=new ArrayList<FriendshipRequest>();
        list.forEach(request->{
            if( request.getTo().get(0).getId().equals(userID)){
                listPendingFriendshiprequest.add(request);
            }
        });
        return listPendingFriendshiprequest;
    }
//to
    public List<FriendshipRequest> getAllRequestsTo(Long userID){
        Iterable<FriendshipRequest> list = requestRepository.findAll();
        List<FriendshipRequest> listPendingFriendshiprequest=new ArrayList<FriendshipRequest>();
        list.forEach(request->{
            if( request.getFrom().getId().equals(userID) && request.getStatus().equals("pending")){
                listPendingFriendshiprequest.add(request);
            }
        });
        return listPendingFriendshiprequest;
    }
    public FriendshipRequest findOne(Long id){
        return requestRepository.findOne(id);
    }

    public Iterable<FriendshipRequest> getAll(){
        return requestRepository.findAll();
    }

    public void updateFriendshipRequest(FriendshipRequest friendshipRequest,String status){
        FriendshipRequest fr=deleteRequest(friendshipRequest.getId());
        fr.setStatus(status);
        fr.setDate(LocalDateTime.now());
        fr=addRequest(fr);
        notifyObserver(new FriendshipRequestChangheEvent(ChangeEventType.UPDATE,fr));
    }

    @Override
    public void addObserver(Observer<FriendshipRequestChangheEvent> e) {
        observers.add(e);

    }

    @Override
    public void removeObserver(Observer<FriendshipRequestChangheEvent> e) {
        //observers.remove(e)

    }

    @Override
    public void notifyObserver(FriendshipRequestChangheEvent t) {
        observers.stream().forEach(x->x.update(t));

    }


}
