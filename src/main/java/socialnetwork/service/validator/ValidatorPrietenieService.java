package socialnetwork.service.validator;

import socialnetwork.domain.validators.ValidationException;

/**
 * define a class that implements a generic interface, Validator
 * @param <T> -
 */
public class ValidatorPrietenieService<T> implements Validator<T> {

    /**
     * @param entity - entity of type T
     * @throws ValidationException if the entity already exists
     */
    @Override
    public void validateAdd(T entity) throws ValidationException {
        if(entity!=null){
            throw new ValidationException("Prietenia exista deja!\n");
        }
    }

    /**
     * @param entity - entity of type T
     * @throws ValidationException if the entity to be deleted does not exist
     */
    @Override
    public void validateDelete(T entity) throws ValidationException {
        if(entity==null){
            throw new ValidationException(("Prietenia ce trebuie stearsa nu exista!\n"));
        }
    }
}
