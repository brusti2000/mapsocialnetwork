package socialnetwork.service.validator;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.domain.validators.ValidationException;

public class ValidatorFriendshipRequestService implements Validator<FriendshipRequest> {



    public void validateFriendshipRequestAlreadyExists(FriendshipRequest friendshipRequestToBeChecked,
                                                        Iterable<FriendshipRequest> friendshipRequestList) throws ValidationException{
        friendshipRequestList.forEach(friendshipRequest -> {
            if (friendshipRequest.getFrom().getId().equals(friendshipRequestToBeChecked.getFrom().getId()) &&
                    friendshipRequest.getTo().get(0).getId().equals(friendshipRequestToBeChecked.getTo().get(0).getId())) {
                throw new ValidationException("The friendship request already exists!");
            }
        });
    }


    public void validateFriendshipAlreadyExists(FriendshipRequest friendshipRequestToBeChecked,
                                                 Iterable<Prietenie> friendshipList) throws ValidationException {
        if (friendshipRequestToBeChecked.getStatus().equals("pending")) {
            friendshipList.forEach(friendship -> {
                if (friendship.getId().getLeft().equals(friendshipRequestToBeChecked.getFrom().getId()) &&
                        friendship.getId().getRight().equals(friendshipRequestToBeChecked.getTo().get(0).getId()))
                    throw new ValidationException("The friendship already exists!");
                if (friendship.getId().getLeft().equals(friendshipRequestToBeChecked.getTo().get(0).getId()) &&
                        friendship.getId().getRight().equals(friendshipRequestToBeChecked.getFrom().getId()))
                    throw new ValidationException("The friendship already exists!");
            });
        };
    }


    public void validateBeforeAdding(FriendshipRequest friendshipRequestToBeChecked,
                                     Iterable<FriendshipRequest> friendshipRequestsList, Iterable<Prietenie> friendshipsList) throws ValidationException{
        validateFriendshipRequestAlreadyExists(friendshipRequestToBeChecked, friendshipRequestsList);
        validateFriendshipAlreadyExists(friendshipRequestToBeChecked, friendshipsList);
    }

    @Override
    public void validateAdd(FriendshipRequest entity) throws ValidationException {
        if(entity!=null){
            throw new ValidationException("Friendship request allready exist!\n");
        }
    }

    @Override
    public void validateDelete(FriendshipRequest entity) throws ValidationException {
        if(entity==null){
            throw new ValidationException(("Friendship request does not exist!\n"));
        }
    }
}
