package socialnetwork.service.validator;

import socialnetwork.domain.validators.ValidationException;

/**
 * define a generic interface
 * @param <T> -
 */
public interface Validator<T> {
    void validateAdd(T entity) throws ValidationException;
    void validateDelete(T entity) throws ValidationException;


}
