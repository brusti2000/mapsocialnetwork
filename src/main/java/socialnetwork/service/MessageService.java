package socialnetwork.service;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.Message;
import socialnetwork.repository.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageService {
    private Repository<Long, Message> repositoryMessage;

    public MessageService(Repository<Long, Message> repositoryMessage) {

        this.repositoryMessage = repositoryMessage;
    }
    public Message addMessage(Message messageParam){
        Message message = repositoryMessage.save(messageParam);
        return message;
    }

    /**
     * @param id - the id of the user
     * @return a list that contains only those messages addressed to the user that has id
     */
    public Iterable<Message> getMessageToUser(Long id){
        Iterable<Message> listIterableAllMessage=repositoryMessage.findAll();
        List<Message> filterList=new ArrayList<>();
        listIterableAllMessage.forEach(message->{
            List<Utilizator> listUsersTo = message.getTo();
            AtomicBoolean b=new AtomicBoolean(false);
            listUsersTo.forEach(users->{
                if(users.getId().equals(id)){
                    b.set(true);
                }
            });
            if(b.get()){
                filterList.add(message);
            }
        });
        return filterList;
    }
    public Message getMessage(Long idMessage){
        return repositoryMessage.findOne(idMessage);
    }
}
