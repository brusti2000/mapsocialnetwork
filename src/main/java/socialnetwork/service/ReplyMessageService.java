package socialnetwork.service;

import socialnetwork.domain.message.ReplyMessage;
import socialnetwork.repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class ReplyMessageService {
    private final Repository<Long, ReplyMessage> replyMessageRepository;

    public ReplyMessageService(Repository<Long, ReplyMessage> replyMessageRepository) {
        this.replyMessageRepository = replyMessageRepository;
    }

    public ReplyMessage addMessage(ReplyMessage replyMessageParam){
        ReplyMessage replyMessage=replyMessageRepository.save(replyMessageParam);
        return replyMessage;
    }

    public ReplyMessage getReplyMessage(Long idReplyMessage){
        return replyMessageRepository.findOne(idReplyMessage);
    }

    public Iterable<ReplyMessage> getConversation(Long idLeftUser, Long idRightUser){
        Iterable<ReplyMessage> listReplyMessages=replyMessageRepository.findAll();
        List<ReplyMessage> conversation=new ArrayList<>();
        listReplyMessages.forEach(replyMessage -> {
            if(replyMessage.getFrom().getId().equals(idLeftUser) &&
                    replyMessage.getTo().get(0).getId().equals(idRightUser))
                conversation.add(replyMessage);
            else if(replyMessage.getFrom().getId().equals(idRightUser)&& replyMessage.getTo().get(0).getId().equals(idLeftUser))
                conversation.add(replyMessage);
        });
        return conversation;
    }
}
