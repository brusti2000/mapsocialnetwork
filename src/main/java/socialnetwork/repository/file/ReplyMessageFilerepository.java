package socialnetwork.repository.file;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.ReplyMessage;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Constants;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReplyMessageFilerepository extends AbstractFileRepository<Long,ReplyMessage>{



    /**
     * @param fileName  - the file from where we load data and to where we upload data
     * @param validator -
     * @param loadData
     * @param repositoryUtilizator
     */
    public ReplyMessageFilerepository(String fileName, Validator<ReplyMessage> validator, boolean loadData, Repository<Long, Utilizator> repositoryUtilizator) {
        super(fileName, validator, true,repositoryUtilizator);

    }


    @Override
    public ReplyMessage extractEntity(List<String> attributes) {

        Long idSender=Long.parseLong(attributes.get(1));
        Long idReceiver=Long.parseLong(attributes.get(2));
        String textMessage=attributes.get(3);
        LocalDateTime data= LocalDateTime.parse(attributes.get(4),Constants.DATE_TIME_FORMATER);
        Long idReplyMessage = Long.parseLong(attributes.get(5));

        return new ReplyMessage(userRepository.findOne(idSender), Arrays.asList(userRepository.findOne(idReceiver)),textMessage,data,findOne(idReplyMessage));
    }

    @Override
    protected String createEntityAsString(ReplyMessage entity) {
        String listTo = "";
        List<Utilizator> listUtilizatori = entity.getTo();
        for(Utilizator utilizator : listUtilizatori){
            listTo += utilizator.getId()+",";
        }
        if(listTo.length()>=1)
            listTo=listTo.substring(0, listTo.length()-1);

        String messageAttributes ="";
        messageAttributes += entity.getId()+";"
                +entity.getFrom().getId()+";"
                +listTo+";"
                +entity.getMessage()+";"
                +entity.getDate().format(Constants.DATE_TIME_FORMATER)+";";
        if(entity.getMessageReply()==null){
            messageAttributes +="0";
        }
        else{
            messageAttributes+=entity.getMessageReply().getId();
        }

        return messageAttributes;

    }
}
