package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * define a class that
 * extends AbstractFileRepository
 * uses a RepositoryUser
 *
 */
public class PrietenieFile extends AbstractFileRepository<Tuple<Long, Long>,Prietenie>{
    private Repository<Long, Utilizator> repositoryUser;

    /**
     * @param fileName -string, represents the name of the file
     * @param validator- represents the object which validates the Prietenie
     * @param repositoryUser- represents the repository
     *  creates the friendship list for each user
     */
    public PrietenieFile(String fileName, Validator<Prietenie> validator, Repository<Long, Utilizator> repositoryUser,boolean loadData) {
        super(fileName, validator,true);
        this.repositoryUser = repositoryUser;
        createListePrietenii();
    }

    /**
     * a method which read a list of string from file and convert it into a frindship
     * @param attributes - list of strings
     * @return prietenie
     */
    @Override
    public Prietenie extractEntity(List<String> attributes) {
        Prietenie prietenie=new Prietenie(LocalDate.parse(attributes.get(0)));
        Long leftId=Long.parseLong((attributes.get(1)));
        Long rightId=Long.parseLong((attributes.get(2)));
        prietenie.setId(new Tuple(leftId,rightId));
        return prietenie;
    }

    /**
     * @param entity - template method design pattern
     * @return an entity of type string
     */
    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getDate()+";"+entity.getId().getLeft() + ";" + entity.getId().getRight();
    }

    /**
     * method that creates the friendship list for each user
     */
   public void createListePrietenii(){
        //entities contine prieteniile 3;4
        entities.forEach((ids,element)->{
            Utilizator utilizatorLeft = repositoryUser.findOne(ids.getLeft());  //utilizatorul cu id ul din partea stanga
            Utilizator utilizatorRight = repositoryUser.findOne(ids.getRight());
            utilizatorLeft.getFriends().add(utilizatorRight); //adaugi in lista de prieteni pe utilizator right=> adica 3 il primeste prieten pe 4
            utilizatorRight.getFriends().add(utilizatorLeft); // 4 il primeste c prieten pe 3
        });
    }
}




