package socialnetwork.repository.file;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.Message;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageFileRepository extends AbstractFileRepository<Long, Message>{


    public MessageFileRepository(String fileName, Validator<Message> validator, boolean loadData, Repository<Long,Utilizator> utilizatorRepository) {
        super(fileName, validator,true,utilizatorRepository);
    }

    @Override
    public Message extractEntity(List<String> attributes) {
//        Message message=new Message(new Utilizator("Funny","Funny"),new ArrayList(),"Funny", LocalDateTime.now());
//        return message;

        Long idUserEmitator=Long.parseLong(attributes.get(1));
        String list=attributes.get(2);
        String[] parts=list.split(",");
        List<Utilizator> idUsersReceptors=new ArrayList<>();
        for(String p:parts){
            Long id=Long.parseLong(p);
            Utilizator user = userRepository.findOne(id);
            idUsersReceptors.add(user);
        }
        String textMesaj = attributes.get(3);
        LocalDateTime data=LocalDateTime.parse(attributes.get(4),Constants.DATE_TIME_FORMATER);
        return new Message(userRepository.findOne(idUserEmitator), idUsersReceptors,textMesaj,data);

    }

    @Override
    protected String createEntityAsString(Message entity) {
        String listTo = "";
        List<Utilizator> listUtilizatori = entity.getTo();
        for(Utilizator utilizator : listUtilizatori){
            listTo += utilizator.getId()+",";
        }
        listTo=listTo.substring(0,listTo.length()-1);
        String messageAttributes ="";
        messageAttributes += entity.getId()+";"
                +entity.getFrom().getId()+";"
                +listTo+";"
                +entity.getMessage()+";"
                +entity.getDate().format(Constants.DATE_TIME_FORMATER);

        return messageAttributes;
    }
}
