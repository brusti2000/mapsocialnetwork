package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.memory.InMemoryRepository;

import java.io.*;

import java.util.Arrays;
import java.util.List;


/**
 * abstract class that - impelements the Tamplete Method Design Pattern
 *                     - and extends InMemoryRepository class
 *  uses a fileName of type String
 * @param <ID> - uniqu identifier
 * @param <E> - entity
 *
 */
///Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factori (vezi mai jos)
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {
    String fileName;
    protected Repository<Long, Utilizator> userRepository;

    /**
     * @param fileName - the file from where we load data and to where we upload data
     * @param validator-
     */
    public AbstractFileRepository(String fileName, Validator<E> validator,boolean loadData) {
        super(validator);
        this.fileName=fileName;
        loadData();
    }


    public AbstractFileRepository(String fileName,Validator<E> validator,boolean loadData, Repository<Long, Utilizator> repository) {
        super(validator);
        this.fileName = fileName;
        this.userRepository = repository;
        loadData();
    }

    /**
     * method in which we load data from file
     */
    private void loadData() {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String linie;
            while((linie=br.readLine())!=null){                                  //citeste fiecare linie
                List<String> attr=Arrays.asList(linie.split(";"));         //imparte atributele
                E e=extractEntity(attr);                                         //creaza entitatea pe baza atributelor
                super.save(e);                                                   //adauga entitatea in repository
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //sau cu lambda - curs 4, sem 4 si 5
//        Path path = Paths.get(fileName);
//        try {
//            List<String> lines = Files.readAllLines(path);
//            lines.forEach(linie -> {
//                E entity=extractEntity(Arrays.asList(linie.split(";")));
//                super.save(entity);
//            });
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    /**
     *  extract entity  - template method design pattern
     *  creates an entity of type E having a specified list of @code attributes
     * @param attributes-
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);
    ///Observatie-Sugestie: in locul metodei template extractEntity, puteti avea un factory pr crearea instantelor entity

    /**
     * @param entity - template method design pattern
     * @return an entity converted to string
     */
    protected abstract String createEntityAsString(E entity);

    /**
     * @param entity entity must be not null
     *               entity must be validated
     *               the id of the entity must be not null
     * @return an entity of type E
     */
    @Override
    public E save(E entity){
        E e=super.save(entity);
        if (e==null) //daca entitatea nu exista  adauga in fisier
        {
            writeToFile(entity);
        }
        return e;

    }

    /**
     * @param id id must be not null
     * @return an entity of type E
     */
    @Override
    public E delete(ID id) {
        E e = super.delete(id);
        if(e!=null){  //daca gaseste entitatea face reload()
            this.reloadData();
        }
        return e;
    }

    /**
     * @param entity - the entity we want to write to file
     */
    protected void writeToFile(E entity){
        try (BufferedWriter bW = new BufferedWriter(new FileWriter(fileName,true))) {
            bW.write(createEntityAsString(entity));
            bW.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *delete the whole file and rewrite the file with the entities that have to remain
     */
    private void reloadData()  {
        Iterable<E> mapEntities = findAll();
        try{
            PrintWriter printWriter=new PrintWriter(fileName);
            printWriter.print("");                              //sterge tot din fisier
            printWriter.close();
        }catch(FileNotFoundException e){
            System.err.println("Fisierul nu exista!");
        }

        mapEntities.forEach((e)->this.writeToFile(e));          //scrie toate entitatile din nou

    }


}

