package socialnetwork.repository.file;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.domain.validators.Validator;

import java.util.List;

/**
 * define a class that
 * extends AbstractFileRepository
 * ID = Long
 * E = Utilizator
 */
public class UtilizatorFile extends AbstractFileRepository<Long, Utilizator>{

    /**
     * @param fileName  - string, represents the name of the file
     * @param validator -  represents the object which validates the Prietenie
     */
    public UtilizatorFile(String fileName, Validator<Utilizator> validator,boolean loadData) {
        super(fileName, validator,true);
    }

    /**
     * a method which read a list of string from file and convert it into a user
     * @param attributes - list of String
     * @return an user
     */
    @Override
    public Utilizator extractEntity(List<String> attributes) {
        Utilizator user = new Utilizator(attributes.get(1),attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));

        return user;
    }

    /**
     * @param entity - template method design pattern
     * @return an entity of type string
     */
    @Override
    protected String createEntityAsString(Utilizator entity) {
        return entity.getId()+";"+entity.getFirstName()+";"+entity.getLastName();
    }
}
