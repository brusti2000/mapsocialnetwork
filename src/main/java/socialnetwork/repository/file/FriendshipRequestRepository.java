package socialnetwork.repository.file;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.domain.message.ReplyMessage;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class FriendshipRequestRepository extends AbstractFileRepository<Long, FriendshipRequest> {

    public FriendshipRequestRepository(String fileName, Validator<FriendshipRequest> validator, boolean loadData, Repository<Long, Utilizator> repository) {
        super(fileName, validator, loadData, repository);
    }

    @Override
    public FriendshipRequest extractEntity(List<String> attributes) {
        Long idSender=Long.parseLong(attributes.get(1));
        Long idReceiver=Long.parseLong(attributes.get(2));
        String textMessage=attributes.get(3);
        LocalDateTime data= LocalDateTime.parse(attributes.get(4), Constants.DATE_TIME_FORMATER);
        String status=attributes.get(5);

        return new FriendshipRequest(userRepository.findOne(idSender), Arrays.asList(userRepository.findOne(idReceiver)),textMessage,data,status);

    }
//id;from;to(List);message;data;status
    @Override
    protected String createEntityAsString(FriendshipRequest entity) {

        String listTo = "";
        List<Utilizator> listUtilizatori = entity.getTo();
        for(Utilizator utilizator : listUtilizatori){
            listTo += utilizator.getId()+",";
        }
        if(listTo.length()>=1)
            listTo=listTo.substring(0, listTo.length()-1);
        //listTo=listTo.substring(0,listTo.length()-1);
        String messageAttributes ="";
        messageAttributes += entity.getId()+";"
                +entity.getFrom().getId()+";"
                +listTo+";"
                +entity.getMessage()+";"
                +entity.getDate().format(Constants.DATE_TIME_FORMATER)+";"
                +entity.getStatus();

        return messageAttributes;
    }
}
