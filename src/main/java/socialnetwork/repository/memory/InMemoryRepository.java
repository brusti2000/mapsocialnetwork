package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * define a class that implements the Repository Interface
 * uses a Validator and a Map of Entities
 * @param <ID>  - unique identifier of the Entity
 * @param <E>   - extends Entity
 */
public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {

    private Validator<E> validator;
    protected Map<ID,E> entities;

    /**
     * @param validator -
     * defines a HashMap of entities
     */
    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities=new HashMap<ID,E>();
    }

    /**
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the entity that has the unique identifier the id given as parameter
     *          throws IllegalArgumentException if the id given as parameter is null
     */
    @Override
    public E findOne(ID id){
        if (id==null)
            throw new IllegalArgumentException("id must be not null");
        return entities.get(id);
    }

    /**
     * @return all the entities from memory as an Iterable
     */
    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     * @param entity entity must be not null
     *               entity must be validated
     *               the id of the entity must be not null
     * @return entity, if the entity already exists
     *          null, if the entity does not exists
     *
     */
    @Override
    public E save(E entity) {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        if(entities.get(entity.getId()) != null) {
            return entity;
        }
        else entities.put(entity.getId(),entity);
        return null;
    }

    /**
     * @param id id must be not null
     * @return  null   , if the entity does not exists
     *          entity , if the entity does  exists
     */
    @Override
    public E delete(ID id) {
        return entities.remove(id);
    }

    /**
     * @param entity entity must not be null
     *               must be validated
     * the entity which has the same id as the entity guven as parameter will be replaced with the one given by parameter
     * @return null, if there is an entity with the entity's id given as parameter
     *         entity, if there is no entity with the entity's id given as parameter
     */
    @Override
    public E update(E entity) {

        if(entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);

        entities.put(entity.getId(),entity);

        if(entities.get(entity.getId()) != null) {
            entities.put(entity.getId(),entity);
            return null;
        }
        return entity;

    }

}
