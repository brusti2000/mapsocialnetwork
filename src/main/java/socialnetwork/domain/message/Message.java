package socialnetwork.domain.message;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {

    private static long idMaxMesaj=0;
    private static long idMaxReply=0;
    private static long idMaxFriendshipRequest=0;
    private Utilizator from;
    private List<Utilizator> to;
    private String message;
    private LocalDateTime date;

//replyMessage
    public Message( Utilizator from, List<Utilizator> to, String message, LocalDateTime date,boolean trash) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        idMaxReply++;
        setId(idMaxReply);
    }
//Message
    public Message( Utilizator from, List<Utilizator> to, String message, LocalDateTime date) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        idMaxMesaj++;
        setId(idMaxMesaj);
    }
//FriendshipRequest
    public Message( Utilizator from, List<Utilizator> to, String message, LocalDateTime date,String trash) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        idMaxFriendshipRequest++;
        setId(idMaxFriendshipRequest);
    }


    public Utilizator getFrom() {
        return from;
    }
    public String getFromFirstName(){return from.getFirstName();}
    public String getFromLastName(){return from.getLastName();}


    public List<Utilizator> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }
    public String getDateString(){return date.format(Constants.DATE_TIME_FORMATER);}

    public void setFrom(Utilizator from) {
        this.from = from;
    }

    public void setTo(List<Utilizator> to) {
        this.to = to;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                ", message='" + message + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(from, message1.from) &&
                Objects.equals(to, message1.to) &&
                Objects.equals(message, message1.message) &&
                Objects.equals(date, message1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, message, date);
    }
}
