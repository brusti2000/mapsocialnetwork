package socialnetwork.domain.message;

import socialnetwork.domain.Utilizator;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class ReplyMessage extends Message{

    private Message messageReply;

    public ReplyMessage(Utilizator from, List<Utilizator> to, String message, LocalDateTime date, Message messageReply) {
        super(from, to, message, date,false);
        this.messageReply = messageReply;
    }

    public Message getMessageReply() {
        return messageReply;
    }

    public void setMessageReply(Message messageReply) {
        this.messageReply = messageReply;
    }

    @Override
    public String toString() {
        String messageString =
                        "ReplyMessage{" +
                        "From= "+ getFrom().getFirstName()+" "+getFrom().getLastName()+" "+
                        "To= "+getTo().get(0).getFirstName()+" "+getTo().get(0).getLastName()+" "+
                        "TextMessage= "+getMessage()+" "+
                        "Date= "+getDate().format(Constants.DATE_TIME_FORMATER);
        if(getMessageReply()!=null)
            messageString+="Reply to= "+getMessageReply().getMessage();
        messageString+="}";
        return messageString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ReplyMessage that = (ReplyMessage) o;
        return Objects.equals(messageReply, that.messageReply);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), messageReply);
    }
}
