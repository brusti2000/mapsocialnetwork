package socialnetwork.domain.message;

import socialnetwork.domain.Utilizator;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;

public class FriendshipRequest extends Message{
    private String status;
//trash: pt incrementarea id-ului
    public FriendshipRequest(Utilizator from, List<Utilizator> to, String message, LocalDateTime date, String status) {
        super(from, to, message, date, "for request");
        this.status=status;
    }

    @Override
    public String toString() {

        String messageString =
                "FriendshipRequest{" + "Id request= "+ getId()+
                        "From= "+ getFrom().getFirstName()+" "+getFrom().getLastName()+" "+
                        "To= "+getTo().get(0).getFirstName()+" "+getTo().get(0).getLastName()+" "+
                        "TextMessage= "+getMessage()+" "+
                        "Date= "+getDate().format(Constants.DATE_TIME_FORMATER)+
                        "Status= "+getStatus()+" }";

        return messageString;
    }

    public String getStatus() {
        return status;
    }

  public String getFirstNameFrom(){
        return getFrom().getFirstName();
  }
    public String getLastNameFrom(){
        return getFrom().getLastName();
    }
    public String getFirstNameTo(){
        return getTo().get(0).getFirstName();
    }
    public String getLastNameTo(){
        return getTo().get(0).getLastName();
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
