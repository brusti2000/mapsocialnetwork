package socialnetwork.domain;

import java.util.Objects;


/**
 * Define a Tuple o generic type entities
 * @param <E1> - tuple first entity type = tipul primei entitati
 * @param <E2> - tuple second entity type
 */
public class Tuple<E1, E2> {
    private E1 e1;
    private E2 e2;

    /**
     * @param e1 - first entity
     * @param e2 - first entity
     */
    public Tuple(E1 e1, E2 e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    /**
     * @return the first entity
     */
    public E1 getLeft() {
        return e1;
    }

    /**
     * @param e1 is being modified
     */
    public void setLeft(E1 e1) {
        this.e1 = e1;
    }

    /**
     * @return the second entity
     */
    public E2 getRight() {
        return e2;
    }

    /**
     * @param e2 is being modified
     */
    public void setRight(E2 e2) {
        this.e2 = e2;
    }

    /**
     * @return the first and the second entity as String, separated by commas
     */
    @Override
    public String toString() {
        return "" + e1 + "," + e2;

    }

    /**
     * @param obj-
     * @return true if the entity is equal to the object introduced as parametre
     */
    @Override
    public boolean equals(Object obj) {
        return this.e1.equals(((Tuple) obj).e1) && this.e2.equals(((Tuple) obj).e2);
    }

    /**
     * @return an integer value that is associated with each entity
     */
    @Override
    public int hashCode() {
        return Objects.hash(e1, e2);
    }
}