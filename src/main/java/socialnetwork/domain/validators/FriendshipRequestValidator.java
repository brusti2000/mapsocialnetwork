package socialnetwork.domain.validators;

import socialnetwork.domain.message.FriendshipRequest;

public class FriendshipRequestValidator implements Validator<FriendshipRequest> {
    @Override
    public void validate(FriendshipRequest entity) throws ValidationException {
        String errors="";
        if(!entity.getStatus().equals("pending") && !entity.getStatus().equals("approved")
        && !entity.getStatus().equals("rejected")){
            errors+="Invalid status!\n Introduce a valid status: pending | approved | rejected\n";
        }
        if(errors.length()>0) throw new ValidationException(errors);
    }
}
