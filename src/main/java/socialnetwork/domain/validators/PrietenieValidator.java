package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * define a class that implements Validator
 * and uses a Repository for Users
 */
public class PrietenieValidator implements Validator<Prietenie> {

    private Repository<Long, Utilizator> userRepository;

    /**
     * @param userRepository -
     */
    public PrietenieValidator(Repository<Long, Utilizator> userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * @param entity-
     * @throws ValidationException if ids are the same
     *                             if the left id does not exist
     *                             if the right id does not exist
     */
    @Override
    public void validate(Prietenie entity) throws ValidationException {
            String errors = "";
            if(entity.getId().getRight().equals(entity.getId().getLeft())){
                errors += "The ids are the same!\n";
            }
        Iterable<Utilizator> utilizatorList=this.userRepository.findAll();
        AtomicBoolean leftIdExists = new AtomicBoolean(false);
        AtomicBoolean rightIdExists = new AtomicBoolean(false);
        utilizatorList.forEach(utilizator -> {
            if(utilizator.getId() == entity.getId().getLeft())
                leftIdExists.set(true);
            if(utilizator.getId()==entity.getId().getRight())
                rightIdExists.set(true);
        });
        if(leftIdExists.get()==false){
            errors+="The left id is not valid - doesn't exist!";
        }
        if(rightIdExists.get()==false){
            errors+="The right id is not valid - doesn't exist!";
        }
        if(errors.length()>0) throw new ValidationException(errors);
    }

}
