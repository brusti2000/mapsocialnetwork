package socialnetwork.domain.validators;

import socialnetwork.domain.message.ReplyMessage;

public class ReplyMessageValidator implements Validator<ReplyMessage> {
    @Override
    public void validate(ReplyMessage entity) throws ValidationException {
        String errors="";
        if(entity.getMessage().matches("[ ]*")){
            errors += "The message can't contain only blank spaces!";
        }
        if(errors.length()>=1){
            throw new ValidationException((errors));
        }
    }
}
