package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

/**
 * define a class that implements a Validator
 */
public class UtilizatorValidator implements Validator<Utilizator> {
    /**
     * @param entity of type Utilizator
     * @throws ValidationException if First or last name contains numbers
     *                             if First or Last name is null
     *                             if First name or Last name contains less than 3 letters
     */
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        //TODO: implement method validate
        String errors="";
        if(entity.getFirstName().matches(".*\\d.*") || entity.getLastName().matches(".*\\d.*")){
            errors+="First or last name contains numbers!\n";
        }
        if(entity.getLastName().equals("") ||entity.getFirstName().equals("")){
            errors+="First or Last name is null\n";
        }
        if(entity.getFirstName().length()<3 || entity.getLastName().length()<3){
            errors+="First name or Last name contains less than 3 letters\n";
        }
        if(errors.length()>0)
            throw new ValidationException(errors);

    }
}
