package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.message.Message;
import socialnetwork.repository.Repository;

import java.util.concurrent.atomic.AtomicBoolean;

public class MessageValidator implements Validator<Message> {
    private Repository<Long, Utilizator> repositoryUtilizator;
    @Override
    public void validate(Message entity) throws ValidationException {
        String errors="";
        if(entity.getMessage().length() < 1){
            errors+="Invalid message!\n";
        }

        if(errors.length()>0) throw new ValidationException(errors);
    }
}
