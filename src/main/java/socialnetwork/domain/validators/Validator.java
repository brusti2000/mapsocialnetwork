package socialnetwork.domain.validators;

/**
 * define a generic interface
 * @param <T> -
 */
public interface Validator<T> {
    void validate(T entity) throws ValidationException;
}