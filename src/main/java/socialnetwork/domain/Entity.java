package socialnetwork.domain;

import java.io.Serializable;

/**
 *define a generic class
 *  @param <ID> - unique identifier for each entity
 */
public class Entity<ID> implements Serializable {

    private static final long serialVersionUID = 7331115341259248461L;
    private ID id;

    /**
     * @return the unique identifier for each entity
     */
    public ID getId() {
        return id;
    }

    /**
     * @param id is modified with the value given as parametre
     */
    public void setId(ID id) {
        this.id = id;
    }
}