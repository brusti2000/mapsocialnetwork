package socialnetwork.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Utilizator is a class that extends Entity
 */
public class Utilizator extends Entity<Long>{
    private String firstName;
    private String lastName;
    private List<Utilizator> friends;

    /**
     * @param firstName - String
     * @param lastName  - String
     *                  -ArrayList of friends;
     */
    public Utilizator(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        friends = new ArrayList<>();
    }

    /**
     * @return firstName - String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName : modifies firstName with the value given as parameter
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return lastName - String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName : modifies lastName with the value given as parameter
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return an ArrayList that contains all the users to whom the user is connected
     */
    public List<Utilizator> getFriends() {
        return friends;
    }

    /**
     * @return an Utilizator as a String
     */
    @Override
    public String toString() {

        AtomicReference<String> friendsString = new AtomicReference<>("");
        int numberOfFriends = friends.size();
        AtomicInteger contor = new AtomicInteger();
        friends.forEach(friend -> {
            if (contor.get() < numberOfFriends - 1) {
                friendsString.set(friendsString + friend.getFirstName() + " " + friend.getLastName() + ", ");
            } else {
                friendsString.set(friendsString + friend.getFirstName() + " " + friend.getLastName());
            }
            contor.getAndIncrement();
        });

        return "Utilizator{" +
                "firstName='" + firstName + '\'' +
                "| lastName='" + lastName + '\'' +
                "| friends=" + friendsString +
                '}';

    }

    /**
     * @param o-
     * @return true if the Utilizator is equal to the Object given as a parameter
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriends().equals(that.getFriends());
    }

    /**
     * @return an integer value that is associated with each Utilizator
     */
    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }


}