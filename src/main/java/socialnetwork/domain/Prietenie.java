package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDate;


/**
 * class that extends entity and connects two Utilizatori
 * E=Tuple
 *
 */
public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDate date;

    /**
     * @param date used when we load a friendship from file
     */
    public Prietenie(LocalDate date) {
        this.date = date; //Used when we load the friendship from the friendship file
    }


    /**
     * @param ids used when we add a friendship at run-time
     *            sets the current date
     */
    public Prietenie(Tuple<Long,Long>ids){
        setId(ids);
        this.date=LocalDate.now();    //used when we add a new friendship at run-time
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @return a Friendship converted to String
     */
    @Override
    public String toString() {
        return "Prietenie{" +
                "date=" + date
                +"idLeft= "+super.getId().getLeft() +" "
                + "idRight= "+super.getId().getRight()+
                '}';
    }
}
