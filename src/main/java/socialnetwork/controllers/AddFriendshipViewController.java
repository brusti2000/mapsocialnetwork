package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dtos.UserDTO;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.FriendshipRequestService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddFriendshipViewController {
    ObservableList<UserDTO> model = FXCollections.observableArrayList();
    PrietenieService friendshipService;
    UtilizatorService userService;
    FriendshipRequestService friendshipRequestService;
    UserDTO selectedUserDTO;

    @FXML
    TableView<UserDTO> tableViewStrangers;
    @FXML
    TableColumn<UserDTO, String> tableColumnFirstName;
    @FXML
    TableColumn<UserDTO, String> tableColumnLastName;
    @FXML
    TextField textFieldMessage;

    @FXML
    public void initialize(){
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableViewStrangers.setItems(model);
    }

    public void setFriendshipService(PrietenieService friendshipService) {
        this.friendshipService = friendshipService;
    }



    public void setFriendshipRequestService(FriendshipRequestService friendshipRequestService) {
        this.friendshipRequestService = friendshipRequestService;
    }

    public void setUserService(UtilizatorService userService,UserDTO selectedUserDTO) {
        this.userService = userService;
        this.selectedUserDTO=selectedUserDTO;
        initModel();
    }

    private void initModel(){
        //cream lista de nonfriends
        Iterable<Utilizator> users = userService.getAll();
        List<UserDTO> nonFriends = new ArrayList<>();
        users.forEach(user->{
            if(friendshipService.findOne(selectedUserDTO.getId(), user.getId())==null &&
                    friendshipService.findOne( user.getId(),selectedUserDTO.getId())==null &&
                    !user.getId().equals(selectedUserDTO.getId()))
            {
                UserDTO userDTO = new UserDTO(user.getFirstName(),user.getLastName());
                userDTO.setId(user.getId());
                nonFriends.add(userDTO);
            }
        });
        if(nonFriends.size()==0){
            model.setAll(nonFriends);
            tableViewStrangers.setPlaceholder(new Label("You are a friend of all users!"));
        }
        else{ model.setAll(nonFriends); }

    }

    public void sendFriendshipRequest(){
        UserDTO userToDTO = tableViewStrangers.getSelectionModel().getSelectedItem();
        if(userToDTO != null){
            String message = textFieldMessage.getText();
            Utilizator userFrom = userService.findOne(selectedUserDTO.getId());
            Utilizator userTo = userService.findOne(userToDTO.getId());

            if(message.matches("[ ]*")){
                message = userFrom.getFirstName()+" "+userFrom.getLastName()+" has sent you a friendship request";
            }

            FriendshipRequest friendshipRequest = new FriendshipRequest(userFrom, Arrays.asList(userTo),
                    message, LocalDateTime.now(),"pending");
            try{
                friendshipRequestService.addRequest(friendshipRequest);
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"The friendship request has been sent");
                alert.show();
            }catch(ValidationException validationException){
                Alert alert = new Alert(Alert.AlertType.ERROR,"You have already sent a friendship request to this user!");
                alert.show();
            }
        }
    }
}
