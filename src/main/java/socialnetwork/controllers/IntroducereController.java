package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.dtos.UserDTO;
import socialnetwork.service.FriendshipRequestService;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;

import java.io.IOException;

public class IntroducereController {
    UtilizatorService userService;
    PrietenieService friendshipService;
    FriendshipRequestService friendshipRequestService;
    MessageService messageService;

    public void setIntroductionStage(Stage introductionStage) {
        this.introductionStage = introductionStage;
    }

    Stage introductionStage;

    ObservableList<UserDTO> modelUserDTO = FXCollections.observableArrayList();

    @FXML
    TableView<UserDTO> tableViewUserDTO;

    @FXML
    TableColumn<UserDTO,String> tableColumnFirstName;

    @FXML
    TableColumn<UserDTO,String> tableColumnLastName;

    @FXML
    public void initialize(){
        //incarca in tabel
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("lastName"));
        tableViewUserDTO.setItems(modelUserDTO);

    }

    public void setUserService(UtilizatorService userService, Stage primaryStage) {
        this.userService = userService;
        this.introductionStage=primaryStage;
        modelUserDTO.setAll(this.userService.getAllUserDTO());//incarcam tabelul cu DTO USERS
    }

    public void setFriendshipService(PrietenieService friendshipServce) {
        this.friendshipService = friendshipServce;
    }

    public void selectFriendsUser(){
        UserDTO selectedUserDTO = tableViewUserDTO.getSelectionModel().getSelectedItem();
        //friendshipService.getAllFriendshipUser(selectedUserDTO.getId()).forEach(System.out::println);
        showAccountUserStage(selectedUserDTO);// apeleaza metoda care imi deschide o fereastra cu accountul userului
    }

    private void showAccountUserStage(UserDTO selectedUserDTO){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/AccountUser.fxml"));
            AnchorPane root = loader.load();

            Stage accountUserStage = new Stage();
            accountUserStage.setTitle("User account");
            accountUserStage.initModality(Modality.APPLICATION_MODAL);
            accountUserStage.setOnCloseRequest(event -> {
                introductionStage.show();
                tableViewUserDTO.getSelectionModel().clearSelection();
            });

            Scene scene = new Scene(root);
            accountUserStage.setScene(scene);
            AccountUserController accountUserController=loader.getController();
            //propagam serviceurile
            accountUserController.setAttributes(friendshipService,userService,selectedUserDTO,friendshipRequestService,accountUserStage,messageService);//seteaza toate serviceurile

            accountUserController.setStageBack(introductionStage);
            accountUserController.setStage(accountUserStage);

            introductionStage.hide();
            accountUserStage.show();

        }catch(IOException e){
            e.printStackTrace();
        }
    }


    public void setFriendshipRequestService(FriendshipRequestService friendshipRequestService) {
        this.friendshipRequestService=friendshipRequestService;
    }

    public void setMessageService(MessageService messageService) {
        this.messageService=messageService;
    }
}
