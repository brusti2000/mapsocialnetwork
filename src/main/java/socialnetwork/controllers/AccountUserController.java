package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dtos.UserDTO;
import socialnetwork.service.FriendshipRequestService;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AccountUserController implements Observer<FriendshipChangeEvent> {
    ObservableList<UserDTO> model= FXCollections.observableArrayList();//lista de la service cu getAll
    PrietenieService friendshipService;
    UserDTO selectedUserDTO;
    UtilizatorService utilizatorService;
    FriendshipRequestService friendshipRequestService;
    Stage accountUserStage;
    MessageService messageService;

    /**
     * method that sets the user account stage
     * @param accountUserStage
     */
    public void setAccountUserStage(Stage accountUserStage) {
        this.accountUserStage = accountUserStage;
    }

    @FXML
    TableColumn<UserDTO,String> tableColumnFirstName;
    @FXML
    TableColumn<UserDTO,String> tableColumnLastName;
    @FXML
    TableView<UserDTO> tableViewAccountUsers;
    @FXML
    TableColumn<UserDTO, String> idFriendsList;
    @FXML
   public  void initialize(){
        //incarc datele cu useri dto(friends of the user))
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("lastName"));
        tableViewAccountUsers.setItems(model);
    }

    private void initModel(){
        //modelul va fi reprezentat de list of friends of the users
        Iterable<Prietenie> friendships = this.friendshipService.getAllFriendshipUser(selectedUserDTO.getId());
        List<UserDTO> listFriends = new ArrayList<>();
        friendships.forEach(friendship -> {
            if(friendship.getId().getRight().equals(selectedUserDTO.getId())) {
                listFriends.add(utilizatorService.getUserDTO(friendship.getId().getLeft()));
            }
            else if(friendship.getId().getLeft().equals(selectedUserDTO.getId())){
                listFriends.add(utilizatorService.getUserDTO(friendship.getId().getRight()));
            }
        });
            if(!friendships.iterator().hasNext()){
                model.setAll(listFriends);

                tableViewAccountUsers.setPlaceholder(new Label("You have no added friends"));
            }else{
                model.setAll(listFriends);
            }
    }
    /**
 * a method that sets the services and stages
    */

    void setAttributes(PrietenieService friendshipService, UtilizatorService utilizatorService,
                       UserDTO selectedUserDTO, FriendshipRequestService friendshipRequestService, Stage accountUserStage, MessageService messageService) {
        this.friendshipService = friendshipService;
        this.friendshipService.addObserver(this);
        this.selectedUserDTO = selectedUserDTO;
        this.utilizatorService=utilizatorService;
        this.accountUserStage=accountUserStage;
        this.friendshipRequestService=friendshipRequestService;
        this.messageService=messageService;

        if (selectedUserDTO != null) {
            idFriendsList.setText(selectedUserDTO.getFirstName()+"'s friends");
            initModel();
        }

    }

    public void deleteFriendship(){
        UserDTO userDTO=tableViewAccountUsers.getSelectionModel().getSelectedItem();
        if(userDTO!=null) {
            Long selectedUser = selectedUserDTO.getId();
            Long userID = userDTO.getId();

            Prietenie friendshipLeft = friendshipService.findOne(selectedUser,userID);
            Prietenie friendshipRight = friendshipService.findOne(userID,selectedUser);

            if(friendshipLeft !=null){ friendshipService.deletePrietenie(new Tuple<>(selectedUser,userID)); }
            if(friendshipRight!=null){ friendshipService.deletePrietenie(new Tuple<>(userID,selectedUser)); }
            tableViewAccountUsers.getSelectionModel().clearSelection();//deselectam userul selectat
        }
        else{
            Alert alert=new Alert(Alert.AlertType.ERROR,"Nothing selected");
            alert.show();
        }
    }
    public void addFriendshipRequest(){
       try{
           FXMLLoader loader=new FXMLLoader();
           loader.setLocation(getClass().getResource("/views/addFriendshipView.fxml"));
           AnchorPane root = loader.load();

           Stage addFriendshipRequestStage = new Stage();
           addFriendshipRequestStage.setTitle("Send friendship request");
           addFriendshipRequestStage.setResizable(false);
           addFriendshipRequestStage.initModality(Modality.APPLICATION_MODAL);
            addFriendshipRequestStage.setOnCloseRequest(event -> {
                accountUserStage.show();
            });
           Scene scene = new Scene(root);
           addFriendshipRequestStage.setScene(scene);
           AddFriendshipViewController addFriendshipViewController=loader.getController();
           //propagam serviceurile
           addFriendshipViewController.setFriendshipService(friendshipService);
           addFriendshipViewController.setUserService(utilizatorService,selectedUserDTO);
           addFriendshipViewController.setFriendshipRequestService(friendshipRequestService);
            accountUserStage.hide();
           addFriendshipRequestStage.show();

       }catch(IOException e){
           e.printStackTrace();
       }
    }
    public void viewFriendshipRequests() {
        try {// cat sa apara fereastra
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/friendshipRequestView.fxml"));
            AnchorPane root = loader.load();

            Stage friendshipRequestViewStage = new Stage();
            friendshipRequestViewStage.setScene(new Scene(root));
            friendshipRequestViewStage.setTitle("Friends Request");
            friendshipRequestViewStage.setOnCloseRequest(event -> {//cand inchide friendsrew sa deschida accountUser
                accountUserStage.show();
            });
            FriendshipRequestViewController friendshipRequestViewController=loader.getController();
            //propagam serviceurile noii ferestre
            friendshipRequestViewController.setFriendshipService(friendshipService,selectedUserDTO);
            friendshipRequestViewController.setFriendshipRequestService(friendshipRequestService);
            friendshipRequestViewController.setStageBack(accountUserStage);
           friendshipRequestViewController.setStage(friendshipRequestViewStage);
            friendshipRequestViewController.setStageToIntroduction(introductionStage);
            friendshipRequestViewController.setFriendshipRequestService(friendshipRequestService);
            accountUserStage.hide();

            friendshipRequestViewStage.show();//aici

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
        @Override
    public void update(FriendshipChangeEvent friendshipChangeEvent) {
        initModel();
    }

    public void viewMessages() {
        try {// cat sa apara fereastra
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/messageView.fxml"));
            AnchorPane root = loader.load();

            Stage messagesViewStage = new Stage();
            messagesViewStage.setScene(new Scene(root));
            messagesViewStage.setTitle("Messages");
            messagesViewStage.setOnCloseRequest(event -> {//cand inchide  sa deschida accountUser
                accountUserStage.show();
            });
            MessageViewController messageViewController=loader.getController();
            messageViewController.setFriendshipService(friendshipService);
            messageViewController.setUserSelectedDTO(selectedUserDTO);
            messageViewController.setUserService(utilizatorService);
            messageViewController.setMessageService(messageService);
            messageViewController.setStageBack(accountUserStage);
            messageViewController.setStage(messagesViewStage);
            messageViewController.setStageToIntroduction(introductionStage);
            messageViewController.setMessageService(messageService);

            //propagam serviceurile noii ferestre

            accountUserStage.hide();

            messagesViewStage.show();//aici

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    Stage introductionStage;

    public void setStage(Stage accountUser)
    {
        this.accountUserStage=accountUserStage;
    }

    public void setStageBack(Stage introductionStage){
        this.introductionStage=introductionStage;
    }




    public void backFromAccountUser() {
        introductionStage.show();
        accountUserStage.hide();
    }
}

