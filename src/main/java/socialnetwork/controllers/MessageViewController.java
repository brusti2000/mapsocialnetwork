package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dtos.UserDTO;
import socialnetwork.domain.message.Message;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.observer.Observable;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageViewController {


    ObservableList<UserDTO> modelUnselected = FXCollections.observableArrayList();
    ObservableList<UserDTO> modelSelected = FXCollections.observableArrayList();
    ObservableList<Message>modelInbox=FXCollections.observableArrayList();
    UtilizatorService userService;
    MessageService messageService;
    PrietenieService friendshipService;
    UserDTO selectedUserDTO;
    List<UserDTO>listUsersSelected=new ArrayList<>();
    List<UserDTO>listUsersUnselected=new ArrayList<>();
    @FXML
    TableView<UserDTO> tableViewUnselected;
    @FXML
    TableView<UserDTO> tableViewSelected;
    @FXML
    TableColumn<UserDTO,String> tableColumnFirstNameUnselected;

    @FXML
    TableColumn<UserDTO,String> tableColumnLastNameUnselected;
    @FXML
    TableColumn<UserDTO,String> tableColumnFirstNameSelected;

    @FXML
    TableColumn<UserDTO,String> tableColumnLastNameSelected;
    @FXML
    TextField textFieldMessageCompose;

    @FXML
    TableView<Message>tableViewInbox;
    @FXML
    TableColumn<Message,String>tableColumnFirstName;
    @FXML
    TableColumn<Message,String>tableColumnLastName;
    @FXML
    TableColumn<Message,String>tableColumnMessage;
    @FXML
    TableColumn<Message,String>tableColumnDate;
    @FXML
    TextField textFieldMessageInbox;


/**  method that initialize the columns of the table
* */
    public void initialize(){
        //pt compose
        tableColumnFirstNameUnselected.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("firstName"));
        tableColumnLastNameUnselected.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("lastName"));
        tableColumnFirstNameSelected.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("firstName"));
        tableColumnLastNameSelected.setCellValueFactory(new PropertyValueFactory<UserDTO,String>("lastName"));

        //pt inbox
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<Message,String>("FromFirstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<Message,String>("FromLastName"));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("message"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<Message,String>("dateString"));

        tableViewInbox.setItems(modelInbox);
        tableViewSelected.setItems(modelSelected);
        tableViewUnselected.setItems(modelUnselected);
    }

/** method that sets the user service
 *  initialize de columns of the table for Compose
 * */
    public void setUserService(UtilizatorService utilizatorService) {
        this.userService=utilizatorService;
        initModel();
    }


    /** method that sets the message service
     *  initialize de columns of the table for Inbox
     * */
    public void setMessageService(MessageService messageService) {
        this.messageService=messageService;
      //  System.out.println(this.messageService);
        initModelInbox();

    }

    /**method that sets the friendship service
     * */
    public void setFriendshipService(PrietenieService friendshipService) {
        this.friendshipService=friendshipService;
    }

    public void setUserSelectedDTO(UserDTO selectedUserDTO) {
        this.selectedUserDTO=selectedUserDTO;
    }

    /** method that initialize the table view for compose
     * */
    private void initModel(){
        //modelul va fi reprezentat de list of friends of the users
        Iterable<Prietenie> friendships = this.friendshipService.getAllFriendshipUser(selectedUserDTO.getId());
        List<UserDTO> listFriends = new ArrayList<>();
        friendships.forEach(friendship -> {
            if(friendship.getId().getRight().equals(selectedUserDTO.getId())) {
                listFriends.add(userService.getUserDTO(friendship.getId().getLeft()));
            }
            else if(friendship.getId().getLeft().equals(selectedUserDTO.getId())){
                listFriends.add(userService.getUserDTO(friendship.getId().getRight()));
            }
        });
        if(!friendships.iterator().hasNext()){
            modelUnselected.setAll(listFriends);

            tableViewUnselected.setPlaceholder(new Label("You have no added friends"));
        }else{
            modelUnselected.setAll(listFriends);
            refreshTables(listFriends);


        }
    }
    /** method that initialize the table view for Inbox messages
     * */
    private void initModelInbox(){
        Iterable<Message>messages=this.messageService.getMessageToUser(selectedUserDTO.getId());
        List<Message>listMessages=new ArrayList<>();
        messages.forEach(m->{
            listMessages.add(m);
        });
        if(!messages.iterator().hasNext()){
            modelInbox.setAll(listMessages);
            tableViewInbox.setPlaceholder(new Label("You dont have messages"));
        }else{
            modelInbox.setAll(listMessages);
        }
    }

    public void refreshTables(List<UserDTO>listFriends){
        listUsersUnselected.clear();
        listUsersSelected.clear();
        listUsersUnselected.addAll(listFriends);
        modelSelected.setAll(listUsersSelected);
       // tableViewSelected.setItems(modelSelected); //modelSelected e observabil devi nu mai trebuie sa scriu linia asta
        //tableViewUnselected.setItems(modelUnselected);
    }

    public void addUserForMessage() {
        UserDTO userDTO=tableViewUnselected.getSelectionModel().getSelectedItem();
        if(userDTO!=null){
            listUsersSelected.add(userDTO);
            modelSelected.setAll(listUsersSelected);
            tableViewSelected.setItems(modelSelected);

            listUsersUnselected.remove(userDTO);
            modelUnselected.setAll(listUsersUnselected);//reincarc lista
            tableViewUnselected.setItems(modelUnselected);
        }
        else{
            Alert alert=new Alert(Alert.AlertType.ERROR,"Nothing selected");
            alert.show();
        }

    }

    public void removeUserFromMessage() {

        UserDTO userDTO=tableViewSelected.getSelectionModel().getSelectedItem();
        if(userDTO!=null){
            listUsersUnselected.add(userDTO);
            modelUnselected.setAll(listUsersUnselected);
            tableViewUnselected.setItems(modelUnselected);
            //tableViewUnselected.getSelectionModel().clearSelection();

            listUsersSelected.remove(userDTO);
            modelSelected.setAll(listUsersSelected);//reincarc lista
            tableViewSelected.setItems(modelSelected);
        }
        else{
            Alert alert=new Alert(Alert.AlertType.ERROR,"Nothing selected");
            alert.show();
        }
    }

    public void sendMessage() {
        String message=textFieldMessageCompose.getText();

        if(!message.matches("[ ]*")){
            if(listUsersSelected.size()!=0){
            List<Utilizator>listUserTo=new ArrayList<>();
            listUsersSelected.forEach(userDTO -> {
                listUserTo.add(userService.findOne(userDTO.getId()));
                    }
            );
            //listUsersSelected.forEach(userDTO -> System.out.println(userDTO));
            Message message1=new Message(userService.findOne(selectedUserDTO.getId()),listUserTo,message, LocalDateTime.now());
            messageService.addMessage(message1);
            textFieldMessageCompose.clear();
            initModel();
            }
            else{
                Alert alert=new Alert(Alert.AlertType.ERROR,"Please select a user to");
                alert.show();
            }
        }else {
            Alert alert=new Alert(Alert.AlertType.ERROR,"Please insert a text message");
            alert.show();
        }

    }



    public void replyMessage() {
        String replyMessage=textFieldMessageInbox.getText();


        Utilizator user= userService.findOne(selectedUserDTO.getId());
        Message selectedMessage=tableViewInbox.getSelectionModel().getSelectedItem();


        if(selectedMessage!=null)
        {
            if(!replyMessage.matches("[ ]*")){
                Message response= new Message(user, Arrays.asList(selectedMessage.getFrom()),replyMessage,LocalDateTime.now());
                messageService.addMessage(response);
                textFieldMessageInbox.clear();
                tableViewInbox.getSelectionModel().clearSelection();
                Alert alert=new Alert(Alert.AlertType.CONFIRMATION,"Reply message sent");
                alert.show();


            }else{
                Alert alert=new Alert(Alert.AlertType.ERROR,"Please insert a text message");
                alert.show();
            }

        }else{
            Alert alert=new Alert(Alert.AlertType.ERROR,"Nothing selected");
            alert.show();
        }
    }
    Stage accountUserStage;
    Stage messageStage;
    Stage introductionStage;
    /**
     a method that sets the current stage
     * */
    public void setStage(Stage messageStage)
    {
        this.messageStage=messageStage;
    }
    public void setStageToIntroduction(Stage introductionStage){
        this.introductionStage=introductionStage;
    }
    public void setStageBack(Stage accountUserStage){
        this.accountUserStage=accountUserStage;
    }

    /**
     a method that shows the previous stage and hide the current stage (from compose)
     * */
    public void backFromCompose() {
        accountUserStage.show();
        messageStage.hide();
    }
    /**
     a method that shows the previous stage and hide the current stage (from inbox)
     * */
    public void backFromInbox() {
        accountUserStage.show();
        messageStage.hide();
    }

    /**
     a method that shows the introduction stage and hide the current stage (from compose)
     * */
    public void backToIntroduction() {
        introductionStage.show();
        messageStage.hide();
    }

    /**
     a method that shows the introduction stage and hide the current stage (from inbox)
    * */
    public void backToIntroductionFromInbox() {
        introductionStage.show();
        messageStage.hide();
    }
}
