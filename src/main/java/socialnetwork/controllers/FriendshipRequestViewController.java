package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.dtos.UserDTO;
import socialnetwork.domain.message.FriendshipRequest;
import socialnetwork.service.FriendshipRequestService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.FriendshipRequestChangheEvent;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;

public class FriendshipRequestViewController implements Observer<FriendshipRequestChangheEvent> {
    ObservableList<FriendshipRequest> model = FXCollections.observableArrayList();
    // e de friendshipRequest pt ca frienshipReq avem in tabel
    PrietenieService friendshipService;
    UserDTO selectedUserDTO;
    FriendshipRequestService friendshipRequestService;

    public void setFriendshipRequestService(FriendshipRequestService friendshipRequestService) {
        this.friendshipRequestService = friendshipRequestService;
        this.friendshipRequestService.addObserver(this);//tot controleru e observer
        initModel();

    }
    public void setFriendshipService(PrietenieService friendshipService,UserDTO selectedUserDTO) {
        this.friendshipService = friendshipService;
        this.selectedUserDTO=selectedUserDTO;

    }
    @FXML
    TableView<FriendshipRequest> idTableFriendsRequest;
    @FXML
    TableColumn<FriendshipRequest, String> idFirstName;
    @FXML
    TableColumn<FriendshipRequest, String> idLastName;
    @FXML
    TableColumn<FriendshipRequest, String> idMessage;
    @FXML
    TableColumn<FriendshipRequest, String> idSentDate;
    @FXML
    TableColumn<FriendshipRequest, String> idStatus;
    @FXML
    Button idAccept;
    @FXML
    Button idDecline;
    @FXML
    Button buttonUnsend;
    @FXML
    Button buttonFrom;
    @FXML
    Button buttonTo;

    @FXML
    TableColumn<FriendshipRequest,String>idTableColumnFromTo;
    @FXML
    public void initialize(){
        idFirstName.setCellValueFactory(new PropertyValueFactory<>("FirstNameFrom"));
        idLastName.setCellValueFactory(new PropertyValueFactory<>("LastNameFrom"));
        idMessage.setCellValueFactory(new PropertyValueFactory<>("message"));
        idSentDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        idStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        idTableFriendsRequest.setItems(model);

    }
    public void initModel(){
        if(idTableColumnFromTo.getText().equals("From")) {
            //toate friendshipRequesturile primite
             model.setAll(friendshipRequestService.getAllRequests(selectedUserDTO.getId()));
        }else
            if(idTableColumnFromTo.getText().equals("To")){
                model.setAll(friendshipRequestService.getAllRequestsTo(selectedUserDTO.getId()));
                //toate friendshipReq trimise
            }
    }


    public void acceptedRequest() {
        FriendshipRequest friendshipRequestSelected=idTableFriendsRequest.getSelectionModel().getSelectedItem();
        if(friendshipRequestSelected.getStatus().equals("pending"))
        {
            friendshipRequestService.updateFriendshipRequest(friendshipRequestSelected,"approved");


            Prietenie prietenie=new Prietenie(new Tuple<>(selectedUserDTO.getId(),friendshipRequestSelected.getFrom().getId()));
            friendshipService.addPrietenie(prietenie);
        }
    }

    @Override
    public void update(FriendshipRequestChangheEvent friendshipRequestChangheEvent) {
        initModel();
    }

    public void rejectedRequest() {
        FriendshipRequest friendshipRequestSelected=idTableFriendsRequest.getSelectionModel().getSelectedItem();//iau friendshipreq selectat
        if(friendshipRequestSelected.getStatus().equals("pending"))
        {
            friendshipRequestService.updateFriendshipRequest(friendshipRequestSelected,"rejected");


        }

    }

    public void handleHideTo() {//cand apas pe from
        buttonUnsend.setVisible(false);
        idAccept.setVisible(true);
        idDecline.setVisible(true);
        idTableColumnFromTo.setText("From");
        idFirstName.setCellValueFactory(new PropertyValueFactory<>("FirstNameFrom"));
        idLastName.setCellValueFactory(new PropertyValueFactory<>("LastNameFrom"));
        initModel();
    }

    public void handleHideFrom() {

        buttonUnsend.setVisible(true);
        idAccept.setVisible(false);
        idDecline.setVisible(false);
        idTableColumnFromTo.setText("To");
        idFirstName.setCellValueFactory(new PropertyValueFactory<>("FirstNameTo"));
        idLastName.setCellValueFactory(new PropertyValueFactory<>("LastNameTo"));
        initModel();
    }

    public void deleteFriendshipRequest() {
        FriendshipRequest friendshipRequestSelected=idTableFriendsRequest.getSelectionModel().getSelectedItem();
        if(friendshipRequestSelected!=null){
            friendshipRequestService.deleteRequest(friendshipRequestSelected.getId());
        }else{
            Alert alert=new Alert(Alert.AlertType.ERROR,"Nothing selected");
            alert.show();
        }
    }
    Stage accountUserStage;
    Stage frienshipRequestStage;
    Stage introductionStage;

    public void setStage(Stage frienshipRequestStage)
    {
        this.frienshipRequestStage=frienshipRequestStage;
    }
    public void setStageToIntroduction(Stage introductionStage){
        this.introductionStage=introductionStage;
    }
    public void setStageBack(Stage accountUserStage){
        this.accountUserStage=accountUserStage;
    }

    public void toFirstPage() {
        introductionStage.show();
        frienshipRequestStage.hide();
    }
    public void toPreviousPage() {
        accountUserStage.show();
        frienshipRequestStage.hide();
    }
}
